﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RRIServiceModel.Migrations
{
    public partial class addLOG_APItable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LOG_API",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    REQUEST = table.Column<string>(type: "text", nullable: true),
                    RESPONSE = table.Column<string>(type: "text", nullable: true),
                    STATUS = table.Column<string>(maxLength: 50, nullable: true),
                    REQUEST_DATETIME = table.Column<DateTime>(nullable: true),
                    RESPONSE_DATETIME = table.Column<DateTime>(nullable: true),
                    MODULE_TYPE = table.Column<string>(maxLength: 100, nullable: true),
                    ACTION_TYPE = table.Column<string>(maxLength: 100, nullable: true),
                    USER_ACCOUNT = table.Column<string>(maxLength: 100, nullable: true),
                    IP = table.Column<string>(maxLength: 50, nullable: true),
                    CREATE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    CREATE_DATE = table.Column<DateTime>(nullable: true),
                    UPDATE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    UPDATE_DATE = table.Column<DateTime>(nullable: true),
                    DELETE_BY = table.Column<string>(maxLength: 30, nullable: true),
                    DELETE_DATE = table.Column<DateTime>(nullable: true),
                    TRANSACTION_ID = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LOG_API", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LOG_API");
        }
    }
}
