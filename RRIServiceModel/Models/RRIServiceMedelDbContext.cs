﻿using Microsoft.EntityFrameworkCore;
using RRPlatFormModel.Models;

namespace RRIServiceModel.Models
{
    public class RRIServiceMedelDbContext : DbContext
    {
        public RRIServiceMedelDbContext(DbContextOptions<RRIServiceMedelDbContext> option)
        : base(option)
        { }
        
        public DbSet<LOG_API> LOG_API { get; set; }

        //public DbSet<MT_USER> MT_USER { get; set; }
        //public DbSet<ACTIVITY_CUSTOMER_HISTORY> ACTIVITY_CUSTOMER_HISTORY { get; set; }
        //public DbSet<CUSTOMER_PROFILE> CUSTOMER_PROFILE { get; set; }
        //public DbSet<EDM_D> EDM_D { get; set; }
        //public DbSet<EDM_H> EDM_H { get; set; }
        //public DbSet<LIST_GOOGLE_FORM> LIST_GOOGLE_FORM { get; set; }
        //public DbSet<MT_CUSTOMER_STATUS> MT_CUSTOMER_STATUS { get; set; }
        //public DbSet<MT_EDM_TYPE> MT_EDM_TYPE { get; set; }
        //public DbSet<SOURCE_DATA> SOURCE_DATA { get; set; }
        //public DbSet<TR_APPOINTMENT> TR_APPOINTMENT { get; set; }
        //public DbSet<TR_FOLLOW> TR_FOLLOW { get; set; }
        //public DbSet<CONTRACT_LIST> CONTRACT_LIST { get; set; }
        //public DbSet<TR_WEB_HOOK_HISTORY> TR_WEB_HOOK_HISTORY { get; set; }

        //public virtual DbSet<MT_MENU> MT_MENU { get; set; }
        //public virtual DbSet<MT_MENU_TYPE> MT_MENU_TYPE { get; set; }
        //public virtual DbSet<MT_MENU_GROUP> MT_MENU_GROUP { get; set; }
        //public virtual DbSet<MT_MENU_GROUP_PERMISSION> MT_MENU_GROUP_PERMISSION { get; set; }
        //public virtual DbSet<MT_USER_GROUP> MT_USER_GROUP { get; set; }
        //public virtual DbSet<MT_USER_GROUP_ROLE> MT_USER_GROUP_ROLE { get; set; }
        //public virtual DbSet<MT_USER_MENU_PERMISSION> MT_USER_MENU_PERMISSION { get; set; }
        //public virtual DbSet<MT_MENU_ROLE_PERMISSION> MT_MENU_ROLE_PERMISSION { get; set; }
        //public virtual DbSet<MT_USER_ROLE> MT_USER_ROLE { get; set; }
        //public virtual DbSet<MT_MENU_TYPE_PERMISSION> MT_MENU_TYPE_PERMISSION { get; set; }
        //public virtual DbSet<MT_USER_TYPE> MT_USER_TYPE { get; set; }

        //public virtual DbSet<TR_STAFF_TEAM> TR_STAFF_TEAM { get; set; }



        //public virtual DbSet<MT_QUESTION_TYPE> MT_QUESTION_TYPE { get; set; }
        //public virtual DbSet<MT_QUESTION> MT_QUESTION { get; set; }
        //public virtual DbSet<MT_ANSWER> MT_ANSWER { get; set; }

        // Master Config H,D
        //public virtual DbSet<MT_CONFIG_H> MT_CONFIG_H { get; set; }
        //public virtual DbSet<MT_CONFIG_D> MT_CONFIG_D { get; set; }


        /////  PROVINCE, DISTRICT, SUB DISTRICT
        //public virtual DbSet<MT_PROVINCE> MT_PROVINCE { get; set; }
        //public virtual DbSet<MT_DISTRICT> MT_DISTRICT { get; set; }
        //public virtual DbSet<MT_SUBDISTRICT> MT_SUBDISTRICT { get; set; }

        //public virtual DbSet<MT_VENDOR> MT_VENDOR { get; set; }
        //public virtual DbSet<MT_PERSON> MT_PERSON { get; set; }

        ////TR CALL
        //public virtual DbSet<TR_CALL_H> TR_CALL_H { get; set; }
        //public virtual DbSet<TR_CALL_D> TR_CALL_D { get; set; }

        ////TR COMPANY
        //public virtual DbSet<MT_COMPANY> MT_COMPANY { get; set; }

        //public virtual DbSet<TR_COMPANY_ADDRESS> TR_COMPANY_ADDRESS { get; set; }
        //public virtual DbSet<TR_COMPANY_CA> TR_COMPANY_CA { get; set; }
        //public virtual DbSet<TR_COMPANY_CA_STATUS_HISTORY> TR_COMPANY_CA_STATUS_HISTORY { get; set; }
        //public virtual DbSet<TR_COMPANY_PERSON> TR_COMPANY_PERSON { get; set; }
        //public virtual DbSet<TR_CA_URL> TR_CA_URL { get; set; }
        //public virtual DbSet<TR_COMPANY_REGISTER> TR_COMPANY_REGISTER { get; set; }
        //public virtual DbSet<TR_REGISTER_ADDRESS> TR_REGISTER_ADDRESS { get; set; }

        //// TR DOCUMENT TR_ACTIVITY
        //public virtual DbSet<TR_ACTIVITY> TR_ACTIVITY { get; set; }
        //public virtual DbSet<TR_DOCUMENT_H> TR_DOCUMENT_H { get; set; }
        //public virtual DbSet<TR_DOCUMENT_D> TR_DOCUMENT_D { get; set; }



    }
}
