namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_USER_ROLE : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ROLE_ID { get; set; }

        [Key]
        [Column(Order = 1)]
       
        public int USER_ID { get; set; }


        [StringLength(50)]
        public  string ROLE_NAME { get; set; }


    }
}
