﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ACTIVITY_CUSTOMER_HISTORY : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int CUSTOMER_ID { get; set; }

        public DateTime? APPOINTMENT_DATE { get; set; }

        [StringLength(1000)]
        public string ADDRESS_SURVEY { get; set; }

        [StringLength(200)]
        public string VENDER_NAME { get; set; }

        [StringLength(20)]
        public string VENDER_CODE { get; set; }

        public int STATUS { get; set; }
    }
}
