﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_VENDOR : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VENDOR_ID { get; set; }
        [Required]
        public string VERDOR_NAME { get; set; }
        public int SEQ { get; set; }
        [Column(TypeName = "text")]
        public string REMARK { get; set; }
    }
}
