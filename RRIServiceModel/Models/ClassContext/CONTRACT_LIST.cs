﻿namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class CONTRACT_LIST : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Column(Order = 1)]
        public int CUSTOMER_ID { get; set; }

        [Column(Order = 2)]
        [StringLength(255)]
        public string CONTRACT_NAME { get; set; }

        [StringLength(60)]
        public string CUSTOMER_NO { get; set; }

        [StringLength(100)]
        public string EMAIL { get; set; }

        [StringLength(10)]
        public string TEL { get; set; }

        [StringLength(255)]
        public string POSITION_NAME { get; set; }

        [StringLength(10)]
        public string GROUP_CODE { get; set; }

        public string CHANEL { get; set; }

        public string MAIN_FROM_STATUS { get; set; }

    }
}
