﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONTACT_TYPE : MasterModel
    {
        [Key]
        [StringLength(2)]
        public string CONTACT_TYPE_CODE { get; set; }
        [StringLength(255)]
        public string CONTACT_TYPE_NAME { get; set; }
    }
}
