﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_ACTIVITY : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ACTIVITY_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CA_ID { get; set; }
        public int ACTIVITY_TYPE_CFID { get; set; }
        public DateTime ACTIVITY_DATE { get; set; }
        public int? REF_VENDOR_ID { get; set; }
        [Required]
        public string ACTIVITY_STATUS { get; set; }
        public string REMARK { get; set; }
        public DateTime? CANCEL_DATE { get; set; }
        public int? REF_CALL_H_ID { get; set; }
    }
}
