﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_STATUS : MasterModel
    {
        [Key]
        public long ID { get; set; }
        [StringLength(100)]
        public string PROJECT_CODE { get; set; }
        [StringLength(255)]
        public string STATUS_NAME { get; set; }
    }
}
