﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_SOURCE_DATA_SUB_CATAGORY : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; } 
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long SOURCE_DATA_CATAGORY_ID { get; set; }
        [StringLength(255)]
        public string SUB_CATEGORY_NAME { get; set; }
    }
}
