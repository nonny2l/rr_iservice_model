﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_PERSON : MasterModel
    {
        [Key]
        public int PERSON_ID { get; set; }
        [Required]
        [StringLength(100)]
        public string TITLE_NAME { get; set; }
        [Required]
        [StringLength(100)]
        public string FIRST_NAME { get; set; }
        [Required]
        [StringLength(100)]
        public string LAST_NAME { get; set; }
        [StringLength(255)]
        public string TELEPHONE { get; set; }
        [StringLength(20)]
        public string MOBILE { get; set; }
        [StringLength(13)]
        public string CITIZEN_ID { get; set; }
        [StringLength(20)]
        public string PASSPORT { get; set; }
        [StringLength(255)]
        public string LINE_ID { get; set; }
    }
}
