﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_QUESTION : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QUESTION_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int QUESTION_TYPE_ID { get; set; }
        [StringLength(200)]
        [Required]
        public string QUESTION_DESC { get; set; }
        public int SEQ { get; set; }

    }
}
