﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_SUB_STATUS : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public long ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long STATUS_ID { get; set; }
        [StringLength(100)]
        public string PROJECT_CODE { get; set; }
        [StringLength(255)]
        public string SUB_STATUS_NAME { get; set; }
    }
}
