﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class EDM_H : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int CUSTOMER_ID { get; set; }

        [Column(Order = 2)]
        [StringLength(60)]
        public string CA_NO { get; set; }

        public string PK_TAXI_ID { get; set; }

        public DateTime? SEND_DATE { get; set; }

        public int EMD_TYPE { get; set; }

        public string LAST_EMAIL_STATUS { get; set; }

        public int CONTRACT_ID { get; set; }

        public string EMAIL { get; set; }

    }
}
