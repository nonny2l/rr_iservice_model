﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_PERSON : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int COMP_PERSON_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PERSON_ID { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COMP_ID { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CA_ID { get; set; }
        [StringLength(100)]
        public string DEPARTMENT { get; set; }
        [StringLength(100)]
        public string JOB_POSITION { get; set; }
        [StringLength(100)]
        public string EMAIL { get; set; }
        [StringLength(20)]
        public string CONTACT_TYPE_CFCODE { get; set; }
      
        public int PERSON_SOURCE_CFID { get; set; }
        [StringLength(1)]
        public string IS_MAIN { get; set; }
    }
}
