﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONFIG_H : MasterModel
    {
        [Key]
        public int CONFIG_H_ID { get; set; }
        [StringLength(20)]
        public string CONFIG_H_CODE { get; set; }
        [StringLength(100)]
        public string CONFIG_H_TNAME { get; set; }
        [StringLength(100)]
        public string CONFIG_H_ENAME { get; set; }

    }
}
