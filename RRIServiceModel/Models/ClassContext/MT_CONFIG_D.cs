﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONFIG_D : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CONFIG_D_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CONFIG_H_ID { get; set; }
        [StringLength(60)]
        public string CONFIG_D_CODE { get; set; }
        public int CONFIG_D_SEQ { get; set; }
        public string CONFIG_D_TNAME { get; set; }
        public string CONFIG_D_ENAME { get; set; }
        public int? PARENT_D_ID { get; set; }
    }
}
