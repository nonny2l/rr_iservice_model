﻿namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TR_WEB_HOOK_HISTORY : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string MESSAGE_ID { get; set; }

        public string ACTION_TYPE { get; set; }

        public string EMAIL { get; set; }

        public string SOURCE { get; set; }

        public string DEVICE { get; set; }

        public string SOURCE_TRANSECTION { get; set; }

        public string CAMPAINGN_ID { get; set; }

        public string CAMPAINGN_NAME { get; set; }

        public string DATE_TIME { get; set; }
    }
}
