namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_USER_MENU_PERMISSION : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
     
        public int USER_ID { get; set; }

        [Key]
        [Column(Order = 2)]
      
        public int MENU_ID { get; set; }


    }
}
