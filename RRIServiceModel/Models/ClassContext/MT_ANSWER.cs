﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_ANSWER : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ANSWER_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int QUESTION_ID { get; set; }
        [StringLength(255)]
        public string ANSWER_DESC { get; set; }
        [StringLength(1)]
        public string IS_FOLLOW_CASE { get; set; }
        public int SEQ { get; set; }
    }
}
