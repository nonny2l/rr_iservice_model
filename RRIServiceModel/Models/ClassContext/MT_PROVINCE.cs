﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_PROVINCE : MasterModel
    {
        [Key]
        [StringLength(2)]
        public string PROV_CODE { get; set; }
        [Required]
        [StringLength(100)]
        public string PROV_TNAME { get; set; }
        [StringLength(100)]
        public string PROV_ENAME { get; set; }
        [Required]
        [StringLength(50)]
        public string PROV_REGION { get; set; }
        [StringLength(10)]
        public string ZONE_PEA { get; set; }
    }
}
