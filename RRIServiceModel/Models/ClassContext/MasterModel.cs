﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class MasterModel
    {
     
        [StringLength(1)]
        [Required]
        public string IS_ACTIVE { get; set; }

        [StringLength(20)]
        [Required]
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }

        [StringLength(20)]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

        [StringLength(20)]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
