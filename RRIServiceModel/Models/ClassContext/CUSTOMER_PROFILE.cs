﻿namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class CUSTOMER_PROFILE : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CUSTOMER_ID { get; set; }

        [Column(Order = 1)]
        [StringLength(60)]
        public string CUSTOMER_NO { get; set; }

        [Column(Order = 2)]
        [StringLength(60)]
        public string CA_NO { get; set; }

        [StringLength(1000)]
        public string CUSTOMER_NAME { get; set; }

        [StringLength(4000)]
        public string ADDRESS { get; set; }

        [StringLength(100)]
        public string SUBDISTRICT { get; set; }

        [StringLength(100)]
        public string DISTRICT { get; set; }

        [StringLength(100)]
        public string PROVINCE { get; set; }

        [StringLength(100)]
        public string REGION { get; set; }

        [StringLength(10)]
        public string TEL01 { get; set; }

        [StringLength(10)]
        public string TEL02 { get; set; }

        [StringLength(10)]
        public string TEL03 { get; set; }

        public string AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE { get; set; }

        public string SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE { get; set; }

        [StringLength(255)]
        public string SUB_BUSSINESS_TYPE { get; set; }

        public int ACTIVITY_STATUS { get; set; }

        [StringLength(4000)]
        public string URL { get; set; }

        [Column(TypeName = "text")]
        public string QR_CODE { get; set; }

        [StringLength(10)]
        public string ZONE { get; set; }

        [StringLength(255)]
        public string BUSSINESS_TYPE { get; set; }

    }
}
