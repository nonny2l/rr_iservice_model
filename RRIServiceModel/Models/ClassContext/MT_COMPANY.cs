﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_COMPANY : MasterModel
    {
        [Key]
        public int COMP_ID { get; set; }
        [StringLength(100)]
        [Required]
        public string COMP_NAME { get; set; }
        [StringLength(200)]
        public string BUSINESS_TYPE { get; set; }
        [StringLength(200)]
        public string SUB_BUSINESS_TYPE { get; set; }
        [StringLength(100)]
        public string TELEPHONE { get; set; }
        [StringLength(100)]
        public string MOBILE { get; set; }
        [StringLength(150)]
        public string EMAIL { get; set; }
        [StringLength(255)]
        public string WEBSITE { get; set; }
        [StringLength(10)]
        [Required]
        public string PROJECT_CODE { get; set; }
        public int COMPANY_SOURCE_CFID { get; set; }
        [StringLength(1)]
        [Required]
        public string IS_COMPANY { get; set; }
    }
}
