﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_REGISTER : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int REGIS_ID { get; set; }
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CA_ID { get; set; }
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PERSON_ID { get; set; }
        public decimal? ELECTRIC_PER_MONTH { get; set; }
        [StringLength(100)]
        public string AREA_ROOF_SQM { get; set; }
        public int REGIS_SOURCE_CFID { get; set; }
        public int? REF_CALL_H_ID { get; set; }
    }
}
