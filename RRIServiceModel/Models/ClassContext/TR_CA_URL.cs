﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_CA_URL : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CA_URL_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CA_ID { get; set; }
        [StringLength(50)]
        [Required]
        public string URL_TYPE { get; set; }
        [StringLength(50)]
        public string SUB_URL_TYPE { get; set; }
        [Column(TypeName = "text")]
        [Required]
        public string URL { get; set; }
    }
}
