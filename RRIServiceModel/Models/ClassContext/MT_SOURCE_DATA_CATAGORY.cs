﻿using System.ComponentModel.DataAnnotations;

namespace RRPlatFormModel.Models
{
    public partial class MT_SOURCE_DATA_CATAGORY : MasterModel
    {
        [Key]
        public long ID { get; set; }
        [StringLength(255)]
        public string CATEGORY_NAME { get; set; }
    }
}
