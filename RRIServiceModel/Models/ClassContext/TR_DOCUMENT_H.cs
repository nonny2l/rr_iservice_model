﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_DOCUMENT_H : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DOC_H_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CA_ID { get; set; }
        [Column(TypeName = "text")]
        public string REMARK { get; set; }
    }
}
