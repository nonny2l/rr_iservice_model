﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class LIST_GOOGLE_FORM : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int CAMPIAN_CODE { get; set; }

        public DateTime? CAMPIAN_DATE { get; set; }

        [StringLength(20)]
        public string VENDER_CODE { get; set; }

        [StringLength(200)]
        public string VENDER_NAME { get; set; }

        [StringLength(20)]
        public string VENDER_TEL { get; set; }

    }
}
