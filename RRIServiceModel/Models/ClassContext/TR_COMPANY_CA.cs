﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_CA : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CA_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COMP_ID { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(20)]
        public string STAFF_ID { get; set; }
        [StringLength(20)]
        [Required]
        public string REF_CA_NO { get; set; }
        [StringLength(10)]
        [Required]
        public string CA_TYPE { get; set; }
        public decimal? AVG_ELECTRIC_PER_MONTH { get; set; }
        public decimal? SUM_ELECTRIC_PER_DAY { get; set; }
        public decimal? AREA_ROOF_SQM { get; set; }
        public int? STATUS_MAIN_CFID { get; set; }
        public int? STATUS_SUB_MAIN_CFID { get; set; }
        //[Column(TypeName = "text")]
        //public string QR_CODE { get; set; }
        public DateTime? REGISTER_DATE { get; set; }
        public DateTime? SURVEY_DATE { get; set; }
        public DateTime? DOC_COMPLETE_DATE { get; set; }
        public DateTime? PROPOSAL_TO_PEA_DATE { get; set; }
        public DateTime? PROPOSAL_PEA_APPROVE_DATE { get; set; }
        public DateTime? PROPOSAL_TO_CL_DATE { get; set; }
        public DateTime? CONTRACT_DATE { get; set; }
        public DateTime? INSTALL_DATE { get; set; }
        public DateTime? DELIVER_DATE { get; set; }
        public int? SURVEY_VENDOR_ID { get; set; }
        public int? INSTALL_VENDOR_ID { get; set; }
        public DateTime? CANCEL_DATE { get; set; }
        public int? CANCEL_RESON_ID { get; set; }
        [Column(TypeName = "text")]
        public string CANCEL_REMARK { get; set; }
        public DateTime? SEND_TO_VENDOR_DATE { get; set; }
        [StringLength(3)]
        public string INVESTMENT_TYPE { get; set; }
        public decimal? ELECTRIC_PERCENT_DISCOUNT { get; set; }
        public decimal? EST_INSTALL_KW { get; set; }
        public decimal? EST_AREA_SQM { get; set; }
        public int? CONTRACT_PERIOD_YEAR { get; set; }
    }
}
