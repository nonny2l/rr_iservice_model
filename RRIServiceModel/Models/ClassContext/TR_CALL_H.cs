﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_CALL_H : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CALL_H_ID { get; set; }
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PERSON_ID { get; set; }
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int COMP_ID { get; set; }
        [StringLength(20)]
        [Required]
        public string STAFF_ID { get; set; }
        [StringLength(1)]
        [Required]
        public string CALL_TYPE { get; set; }
        public DateTime CALL_DATE { get; set; }
        [StringLength(50)]
        [Required]
        public string CONTACT_PHONE { get; set; }
        public int CONTACT_CHANNEL_CFID { get; set; }
        [StringLength(1)]
        [Required]
        public string IS_FINISH_CALL { get; set; }

        public int? REF_CA_ID { get; set; }
        
    }
}
