﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_CALL_D : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TR_CALL_D_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TR_CALL_H_ID { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int QUESTION_ID { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ANSWER_ID { get; set; }
        [StringLength(1)]
        public string IS_SERIOUS_CASE { get; set; }
        [StringLength(1)]
        public string IS_FOLLOW_CASE { get; set; }
        [StringLength(1)]
        public string IS_FINISH_CASE { get; set; }
        [StringLength(255)]
        public string REMARK { get; set; }
    }
}
