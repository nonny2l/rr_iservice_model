﻿namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class SOURCE_DATA : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int CUSTOMER_ID { get; set; }

        [StringLength(1000)]
        public string SOURCE_NAME { get; set; }

    }
}
